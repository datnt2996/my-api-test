const axios = require('axios');
const db = require('../db');
const ResultCodes = require('../result-codes');
const { Team, User, Department, MemberOfTeam, UserGroup, Group, TeamOfDepartment } = db;
const { SearchStream } = require('datnt2996/mongo');
const { Kinds } = require('datnt2996');
const Enums = require('../db/enums');

const TeamManager = function (app) {
    this.app = app;
};
module.exports = TeamManager;


TeamManager.prototype.create = async function (team) {
    console.debug(`create(), team:${JSON.stringify(team)}`);

    Kinds.mustBeObject(team);
    console.log('TeamManager', team);
    return this.create([team]);
};

TeamManager.prototype.getById = async function (id) {
    console.debug(`getById(), id:${id}`);

    if (!Kinds.isObjectId(id)) {
        throw ResultCodes.newError('Invalid Object Id', ResultCodes.PARAM_INVALID_VALUE);
    }

    const team = await Team.findById(id);
    if (!team) {
        throw ResultCodes.newError(`Team ${id} doesn't exist`, ResultCodes.NO_CONTENT, { id });
    }
    return team;
};

TeamManager.prototype.find = async function (filter) {
    console.debug(`find(), filter:${JSON.stringify(filter)}`);

    const highFilter = {};
    if (filter.userIds) {
        highFilter._id = { $in: Kinds.asObjectIds(filter.userIds) };
        //return User.find({ _id: { $in: Kinds.asObjectIds(filter.userIds) } }, '_id displayName avatar');
    }

    filter._id && (highFilter._id = { $in: Kinds.asObjectIds(filter._id) });

    if (filter.text) {
        highFilter.$text = { $search: filter.text };
    }

    const query = SearchStream.of(Team);
    query
        .highFilter(highFilter)
        .resultKey(filter.resultKey)
        .limit(Kinds.asNumber(filter.limit, 10))
        .sort(filter.order);

    return query.exec();
};

TeamManager.prototype.update = async function (id, obj) {
    console.debug(`update(), id:${id}`);
    Kinds.mustExist(id);
    Kinds.mustExist(obj);

    const team = await this.getById(id);

    Object.keys(obj).forEach((key) => {
        if (obj[key] === undefined) {
            return;
        }
        if (Kinds.isString(obj[key]) && obj[key].length === 0) {
            return;
        }
        user[key] = obj[key];
    });

    return team.save();
};

TeamManager.prototype.addMember = async function (obj) {
    console.debug(`addMember(), team:${JSON.stringify(obj)}`);
    Kinds.mustBeString(obj.member);
    Kinds.mustBeString(obj.team);
    if (!Kinds.isObjectId(obj.member)) {
        throw ResultCodes.newError('Invalid Object Id', ResultCodes.PARAM_INVALID_VALUE);
    }
    if (!Kinds.isObjectId(obj.team)) {
        throw ResultCodes.newError('Invalid Object Id', ResultCodes.PARAM_INVALID_VALUE);
    }
    console.log('DepartmentManager', obj);
    return MemberOfTeam.create([obj]);
};

TeamManager.prototype.removeMember = async function (_id) {
    console.debug(`removeMember(), team: ${_id}`);
    Kinds.mustBeString(id);
    if (!Kinds.isObjectId(id)) {
        throw ResultCodes.newError('Invalid Object Id', ResultCodes.PARAM_INVALID_VALUE);
    }
    return MemberOfTeam.deleteOne({_id});
};

TeamManager.prototype.getMemberByTeamId = async function (id, filter) {
    console.debug(`getMemberByTeamId(), team:${JSON.stringify(filter)}`);
    Kinds.mustBeString(id);
    if (!Kinds.isObjectId(id)) {
        throw ResultCodes.newError('Invalid Object Id', ResultCodes.PARAM_INVALID_VALUE);
    }
    const highFilter = {
        team:{ $in: Kinds.asObjectId(id)}
    };
    filter.teamIds && (highFilter.team = { $in: Kinds.asObjectIds(filter.teamIds) });


    const query = SearchStream.of(TeamOfDepartment);
    query
        .highFilter(highFilter)
        .lookup({
            from: 'User',
            localField: 'member',
            foreignField: '_id',
            as: 'members'
        })
        .resultKey(filter.resultKey)
        .limit(Kinds.asNumber(filter.limit, 10))
        .sort(filter.order);

    return query.exec();

};