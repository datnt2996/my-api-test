const Jwt = require('../crypto/jwt');
const Kinds = require('../kinds');
const ResultCodes = require('../common-result-codes');

const OppositeOperation = {
    '1': '$gt',
    '-1': '$lt'
};

function deepFind(obj, path) {
    var paths = path.split('.'),
        current = obj,
        i;

    for (i = 0; i < paths.length; ++i) {
        if (current[paths[i]] == undefined) {
            return undefined;
        } else {
            current = current[paths[i]];
        }
    }
    return current;
}

class SearchStream {
    constructor() {
        this._refs = {};
        this._limit = 25;
        this._rawLookups = [];
        this._unwind = [];
        this._distinct = '';
    }

    static of(model) {
        const s = new SearchStream();
        s._model = model;
        return s;
    }

    ref(path, model, doUnwind = true, preserveNullAndEmptyArrays = true, localField = null, foreignField = '_id') {
        this._refs[path] = {
            model,
            doUnwind,
            preserveNullAndEmptyArrays,
            foreignField,
            localField: localField || path
        };
        return this;
    }
    getRawLookups() {
        return this._rawLookups.map((body) => {
            return { $lookup: body };
        });
    }
    distinct(field){
       this._distinct = field;
       return this;
    }
    getDistinct(){
        return [
            {
            '$group': {
               _id: this._distinct,
                "group_data": { "$first": "$$ROOT" }        
                }
            },
            {
                "$replaceRoot": { "newRoot": "$group_data" }
            }
        ];
    }

    lookup(body) {
        this._rawLookups.push(body);
        return this;
    }
    getUnwind(concat) {

        for (let unwind of this._unwind) {
            concat.push(unwind);
        }
        return concat;
    }
    unwind(path, preserveNullAndEmptyArrays = false) {
        this._unwind.push({
            $unwind: {
                path: `$${path}`,
                preserveNullAndEmptyArrays: preserveNullAndEmptyArrays
            }
        });
        return this;
    }

    limit(number) {
        this._limit = number;
        return this;
    }

    unset(unset){
        this._unset = unset;
        return this;
    }

    resultKey(resultKey) {
        this._resultKey = resultKey;
        return this;
    }

    sort(sort) {
        this._sort = sort;
        return this;
    }

    project(project) {
        this._project = project;
        return this;
    }

    geoNear(geoNear) {
        this._geoNear = geoNear;
        return this;
    }

    highFilter(filter) {
        this._hightFilter = filter;
        return this;
    }

    lowFilter(filter) {
        this._lowFilter = filter;
        return this;
    }

    /**
     * @private
     */
    getSort() {
        const sort = { ...this._sort };
        // make sure _id always at the end!
        if (sort._id) {
            const idSort = sort._id;
            delete sort._id;
            sort._id = idSort;
        } else {
            sort._id = -1;
        }
        return sort;
    }

    /**
     * @private
     */
    getLookups(refPaths) {
        const result = [];
        for (const key of refPaths) {
            // const asField = this._refs[key].localField;
            result.push({
                $lookup: {
                    from: this._refs[key].model.collection.name,
                    localField: this._refs[key].localField,
                    foreignField: this._refs[key].foreignField,
                    as: key
                }
            });
            if (this._refs[key].doUnwind) {
                result.push({
                    $unwind: {
                        path: `$${key}`,
                        preserveNullAndEmptyArrays: this._refs[key].preserveNullAndEmptyArrays
                    }
                });
            }
        }
        return result;
    }

    /**
     * IMPORTANT: this is the simple fix, but we expect the better verson below
     */
    getSortedRefPaths() {
        const sortedFields = Kinds.objectKeys(this.getSort());

        const sortedRefPaths = Kinds.objectKeys(this._refs);
        const nonSortedRefPaths = [];

        return { sortedRefPaths, nonSortedRefPaths };
    }

    // /**
    //  * IMPORTANT: getSortedRefPaths must also return path in $math
    //  */
    // getSortedRefPaths() {
    //     const sortedFields = Kinds.objectKeys(this.getSort());

    //     const sortedRefPaths = [];
    //     const nonSortedRefPaths = [];

    //     for (const path of Kinds.objectKeys(this._refs)) {
    //         let found = false;
    //         if (!this._refs[path].preserveNullAndEmptyArrays) {
    //             for (const sortedField of sortedFields) {
    //                 if (sortedField.startsWith(path)) {
    //                     sortedRefPaths.push(path);
    //                     found = true;
    //                     break;
    //                 }
    //             }
    //         }
    //         !found && nonSortedRefPaths.push(path);
    //     }

    //     return { sortedRefPaths, nonSortedRefPaths };
    // }

    /**
     * @private
     */
    buildOldResultFilter(values, arr, index) {
        let value = values[index / 2];
        if (Kinds.isObjectIdString(value)) {
            value = Kinds.asObjectId(value);
        } else if (Kinds.isString(value)) {
            const d = Date.parse(value);
            if (d) {
                value = new Date(d);
            }
        }
        const path = arr[index];
        const order = arr[index + 1];

        if (path === '_id') {
            const opposite = {
                _id: {}
            };
            opposite._id[OppositeOperation[order]] = value;
            return opposite;
        }

        const opposite = {};
        opposite[path] = {};
        opposite[path][OppositeOperation[order]] = value;

        const equal = {};
        equal[path] = value;

        const sub = this.buildOldResultFilter(values, arr, index + 2);
        if (sub._id) {
            equal._id = sub._id;
        } else {
            equal.$or = sub.$or;
        }
        return { $or: [opposite, equal] };
    }

    /**
     * @private
     */
    getOldResultFilter(keyObject, sortObject) {
        const { last } = keyObject;
        Kinds.mustBeArray(last);

        const arr = [];
        for (const key of Kinds.objectKeys(sortObject)) {
            arr.push(key);
            arr.push(sortObject[key]);
        }

        return {
            $match: this.buildOldResultFilter(last, arr, 0)
        };
    }

    /**
     * @private
     */
    generateFilters(timesampe) {
        const high = [];
        const low = [];

        return { highFilter, lowFilter };
    }

    async buildPipeline(timestamp, hasLimit = true) {
        const $sort = this.getSort();
        const $project = this._project;
        let lowFilter = this._lowFilter;
        const { sortedRefPaths, nonSortedRefPaths } = this.getSortedRefPaths();

        const highFilter = { ...this._hightFilter };

        let oldResultFilter;

        if (this._resultKey) {
            const keyBody = await Jwt.decode(this._resultKey);
            if (!keyBody.timestamp || !keyBody.last) {
                throw ResultCodes.newError('Invalid resultKey', ResultCodes.PARAM_INVALID_VALUE);
            }
            timestamp = new Date(keyBody.timestamp);

            if (keyBody.last.length > 1) {
                oldResultFilter = this.getOldResultFilter(keyBody, $sort);
            } else if (keyBody.last.length === 1) {
                highFilter._id = {};
                highFilter._id[OppositeOperation[$sort._id]] = Kinds.asObjectId(keyBody.last[0]);
            } else {
                throw new Error('Invalid ResultKey');
            }
        }

        highFilter.updatedAt = { $lt: timestamp };

        let pipeline = [];

        if (this._geoNear) {
            pipeline.push({ $geoNear: this._geoNear });
        }
        pipeline.push({ $match: highFilter });

        highFilter.$text && pipeline.push({ $addFields: { __textScore: { $meta: 'textScore' } } });
        const concat = this.getUnwind(this.getRawLookups());
        pipeline = pipeline.concat(concat);


        if (sortedRefPaths.length > 0) {
            pipeline = pipeline.concat(this.getLookups(sortedRefPaths));
            lowFilter = lowFilter || {};
            for (const key of sortedRefPaths) {
                lowFilter.$or = lowFilter.$or || [];
                const s = {};
                if (this._refs[key].doUnwind) {
                    s[key] = { $exists: false };
                } else {
                    s[key] = { $size: 0 };
                }
                lowFilter.$or.push(s);
                const found = {};
                found[key + '.updatedAt'] = { $lt: timestamp };
                lowFilter.$or.push(found);
            }
        }
        if(this._distinct){
            pipeline = pipeline.concat(this.getDistinct());
        }

        oldResultFilter && pipeline.push(oldResultFilter);
        lowFilter && pipeline.push({ $match: lowFilter });

        pipeline.push({ $sort });

        if (hasLimit) {
            pipeline.push({ $limit: this._limit });
        }

        if (nonSortedRefPaths.length > 0) {
            pipeline = pipeline.concat(this.getLookups(nonSortedRefPaths));
        }

        if(this._unset){
            pipeline.push({ $unset: this._unset})
        }

        if ($project) {
            // must retrieve all sorted field.
            for (const key of Kinds.objectKeys($sort)) {
                if (!$project[key] && !key.includes('.')) {
                    $project[key] = 1;
                }
            }
            pipeline.push({ $project });
        }

        return pipeline;
    }

    async exec() {
        let timestamp = new Date();
        const $sort = this.getSort();
        const pipeline = await this.buildPipeline(timestamp, true);
        console.log(Kinds.toString(pipeline));
        const data = await this._model.aggregate(pipeline);
        let resultKey;
        if (data && data.length === this._limit) {
            const lastResult = data[data.length - 1];

            const last = [];

            for (const key of Kinds.objectKeys($sort)) {
                last.push(deepFind(lastResult, key));
            }

            resultKey = await Jwt.encode({ timestamp: timestamp.getTime(), last });
        }

        return { data, resultKey };
    }

    async cursor() {
        let timestamp = new Date();
        const pipeline = await this.buildPipeline(timestamp, false);
        console.log(Kinds.toString(pipeline));
        return this._model
            .aggregate(pipeline)
            .cursor({ batchSize: 100 })
            .exec();
    }
}

module.exports = SearchStream;

/**
 SearchChunk(db.Class)
 .ref('lesson', db.Lesson)
 .limit(15)
 .sortBy({$score: 1, title: 1})
 .resultKey(resultKey)
 .project({name: 1}); */
