const ResultCodes = require('../common-result-codes');
const Kinds = require('../kinds');

const { PassThrough, Writable } = require('stream');

function toCsvValue(value) {
    let str = `${JSON.stringify(value)}`;

    if (str.charAt('0') === '"' && str.charAt(str.length - 1) === '"') {
        str = str.substr(1, str.length - 2);
    }
    str = str.replace(/\"/g, '""');

    if (/[",]/.test(str)) {
        return `"${str}"`;
    }

    return str;
}
function getCsvHeaderString(fields) {
    const values = fields.map((field) => {
        return toCsvValue(field);
    });
    return values.join(', ');
}
function getCsvRowString(fields, object) {
    const values = fields.map((field) => {
        return toCsvValue(object[field]);
    });
    return values.join(',');
}
async function toCsvStream(ctx, cursor) {

    const pass = new PassThrough();
    const writable = new Writable();
    pass.pipe(writable);
    pass.unpipe(writable);
    ctx.body = pass;

    let fields = null;

    await cursor.eachAsync(function (doc) {
        if (!fields) {
            fields = Object.keys(doc);
            pass.write(getCsvHeaderString(fields));
        }
        pass.write('\n');
        pass.write(getCsvRowString(fields, doc));

        pass.resume(); // Must be called to make stream emit 'data'
    });

    pass.end();
    console.log('done');
}
async function asCsvStream(ctx, array) {
    const pass = new PassThrough();
    const writable = new Writable({
        write(chunk, encoding, callback) {
            console.log(chunk.toString());
            callback();
        }
    });
    pass.pipe(writable);
    pass.unpipe(writable);
    ctx.body = pass;

    let fields = null;
    array.forEach(function (doc) {
        if (!fields) {
            fields = Object.keys(doc);
            pass.write(getCsvHeaderString(fields));
        }
        pass.write('\n');
        pass.write(getCsvRowString(fields, doc));

        pass.resume(); // Must be called to make stream emit 'data'
    });
    console.log("pass final data .........");
    console.log(pass);
    pass.end();
    console.log('done');
}
async function exportCsv(ctx, array) {
    let dataExport = '';
    let fields = null;
    array.forEach(function (doc) {
        if (!fields) {
            fields = Object.keys(doc);
            console.log(fields);
            dataExport += getCsvHeaderString(fields);
        }
        dataExport += '\n';
        dataExport += getCsvRowString(fields, doc);
    });
    ctx.body = dataExport;
    console.log('done');
}

class Res {
    constructor(ctx) {
        this.ctx = ctx;
    }

    end(status, body) {
        this.ctx.response.status = status;
        this.ctx.response.body = body;
    }

    send(httpStatus, code, message, data, meta) {
        let payload;
        if (Kinds.isSet(data)) {
            payload = Kinds.isArray(data) ? data : [data];
        }

        this.end(httpStatus, {
            code,
            message,
            results: payload,
            meta
        });
    }

    ok(message, data, meta) {
        this.send(200, ResultCodes.SUCCESS, message, data, meta);
    }

    bad(code, message, data) {
        // this.send(400, code, message, data, 400);
        this.send(200, code, message, data); // be simple
    }

    badParams(message, fields) {
        this.bad(ResultCodes.PARAM_INVALID, message, fields);
    }

    badParamValues(message, fields) {
        this.bad(ResultCodes.PARAM_INVALID_VALUE, message, fields);
    }

    invalidState(message, data) {
        this.send(200, ResultCodes.INVALID_STATE, message, data); // be simple
        // this.send(422, ResultCodes.INVALID_STATE, message, data, 400);
    }

    unauthorized() {
        this.send(200, ResultCodes.UNAUTHORIZED, 'Login required'); // be simple
        // this.send(401, ResultCodes.UNAUTHORIZED, 'Login required', 401);
    }

    noContent(message, data) {
        // this.send(204, ResultCodes.NO_CONTENT, message, data);
        this.send(200, ResultCodes.NO_CONTENT, message, data); // be simple
    }

    forbidden(message, data) {
        this.send(200, ResultCodes.FORBIDDEN, message, data); // be simple
    }

    resultCodeOrFail(err) {
        if (err.resultCode) {
            return this.bad(200, err.resultCode, err.message);
        }
        return Promise.reject(err);
    }

    pipeCsv(cursor, fileName = null) {
        if (!fileName) {
            fileName = `Download ${new Date()}.csv`;
        }
        const ctx = this.ctx;

        ctx.set('Content-Type', 'text/csv');
        ctx.set('Content-Disposition', 'attachment; filename="' + fileName);

        toCsvStream(ctx, cursor);
    }

    asCsv(array, fileName = null) {
        console.log(array);
        if (!fileName) {
            fileName = `Download ${new Date()}.csv`;
        }

        const ctx = this.ctx;
        ctx.set('Content-Disposition', 'attachment; filename="' + fileName);
        ctx.set('Content-Type', 'text/csv');

        return exportCsv(ctx, array);

    }

}

module.exports = (ctx) => new Res(ctx);


