const $ = {};
module.exports = $;

function buildManagers(appCtx) {
    return {
        userManager: new (require('./user-manager'))(appCtx),
        teamManager: new (require('./team-manager'))(appCtx),
        departmentManager: new(require('./department-manager'))(appCtx),
        userGroupManager: new(require('./user-group-manager'))(appCtx),
    };
}

/**
 * @property {UserManager} userManager,
 */
/**
 *
 * @param {AppCtx} app
 * @returns {Promise<AppCtx>}
 */
$.load = async (app) => {
    const managers = buildManagers(app);
    managers.build = buildManagers;
    app.managers = managers;
    return app;
};
