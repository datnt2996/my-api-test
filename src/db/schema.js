const mongoose = require('mongoose');
const Enums = require('./enums');
const { Kinds } = require('datnt2996');

const Schema = mongoose.Schema;

const { String, Number, Date, ObjectId, Mixed } = Schema.Types;

const schema = {};
module.exports = schema;

schema.User = new Schema(
    {
        email: {
            type: String,
            required: true,
            // unique: true,
            // sparse: true,
            // index: true,
            trim: true
            // validate: (value) => Kinds.isEmail(value)
        },
        phone: {
            type: String,
            // unique: true,
            // sparse: true,
            // index: true,
            // safe: null,
            // background : true,
            trim: true,
        },
        displayName: {
            type: String,
            trim: true,
            required: true,
        },
        gender: {
            type: String,
            default: Enums.Genders.UNKNOWN,
            enum: Kinds.objectValues(Enums.Genders)
        },
        avatar: String, // name of file save in database
        birthday: Date, // 'YYYY-MM-DDTHH:mm:ss.sssZ', (ex: 1994-11-05T13:15:30Z)'
        role: {
            type: String,
            default: Enums.UserRoles.UNKNOWN,
            enum: Kinds.objectValues(Enums.UserRoles)
        }, 
    },
    {
        collection: 'User',
        timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } // very important in searching
    }
);
schema.User.index({ displayName: 'text', phone: "text", email: "text" });

schema.UserGroup = new Schema(
    {
        user: {
            type: ObjectId,
            required: true,
            ref: 'User'
        },
        group: {
            type: ObjectId,
            required: true,
            ref: 'Group'
        }
    },
    {
        collection: 'UserGroup',
        timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
    }
);
schema.UserGroup.index({ user: 1, group: 1 }, { unique: true });

schema.Group = new Schema(
    {
        name: {
            type: String,
            minLength: 1
        },
        permissions: [
            {
                type: String,
                enum: Kinds.objectValues(Enums.Permissions)
            }
        ]
    },
    {
        collection: 'Group'
    }
);

schema.Department = new Schema({
        name: {
            type: String,
            required: true,
            minLength: 1
        },
        description: String,
        manager: {
            type: ObjectId,
            ref: 'User'
        },
        director:{
            type: ObjectId,
            ref: 'User'
        }
},
{
    collection: 'Department',
    timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
})
schema.Department.index({  name: 'text' });

schema.TeamOfDepartment = new Schema({
    department: {
        type: ObjectId,
        required: true,
        ref: 'Department'
    },
    team: {
        type: ObjectId,
        required: true,
        ref: 'Team'
    }
},
{
    collection: 'TeamOfDepartment',
    timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
})
schema.UserGroup.index({ department: 1, team: 1 }, { unique: true });

schema.Team = new Schema({
    name: {
        type: String,
        minLength: 1
    },
    description: String,
    department: {
        type: ObjectId,
        ref: 'User'
    },
    leader: {
        type: ObjectId,
        ref: 'Department'
    }
},
{
    collection: 'Team',
    timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
})
schema.Team.index({  name: 'text' });

schema.MemberOfTeam = new Schema({
    member: {
        type: ObjectId,
        required: true,
        ref: 'User'
    },
    team: {
        type: ObjectId,
        required: true,
        ref: 'Team'
    }
},
{
    collection: 'MemberOfTeam',
    timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
})

schema.UserGroup.index({ member: 1, team: 1 }, { unique: true });
