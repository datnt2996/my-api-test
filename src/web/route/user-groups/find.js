const { Res } = require('datnt2996/koa');
const Enums = require('../../../db/enums');
const { Kinds } = require('datnt2996');
const ResultCodes = require('../../../result-codes');

module.exports = async (ctx) => {
    const { appCtx } = ctx.state;
    const { userGroupManager } = appCtx.managers;

	return userGroupManager.find(ctx.request.query).then (result => {
        const { resultKey, data } = result;
        Res(ctx).ok('ok', data, { resultKey });
	});
};
