const jwt = require('jsonwebtoken');

const SECRET_KEY = 'jajshdkfkjfhah;skdhdffahsdflkasdff';

class Jwt {
    static async encode(data) {
        return jwt.sign(
            {
                data
            },
            SECRET_KEY
        );
    }

    static async decode(str) {
        return jwt.decode(str, { complete: true }).payload.data;
    }
}

module.exports = Jwt;
