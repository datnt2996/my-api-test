
module.exports = async (ctx) => {
    const { appCtx } = ctx.state;
    const { teamManager } = appCtx.managers;

    return teamManager.create(ctx.request.body);
};
