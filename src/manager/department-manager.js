const axios = require('axios');
const db = require('../db');
const { Team, User, Department, MemberOfTeam, UserGroup, Group, TeamOfDepartment } = db;
const { SearchStream } = require('datnt2996/mongo');
const { Kinds } = require('datnt2996');
const ResultCodes = require('../result-codes');
const Enums = require('../db/enums');

const DepartmentManager = function (app) {
    this.app = app;
};
module.exports = DepartmentManager;


DepartmentManager.prototype.create = async function (obj) {
    console.debug(`create(), team:${JSON.stringify(obj)}`);

    Kinds.mustBeObject(obj);
    return Department.create([obj]);
};

DepartmentManager.prototype.findById = async function (id) {
    console.debug(`findById(), id:${id}`);

    if (!Kinds.isObjectId(id)) {
        throw ResultCodes.newError('Invalid Object Id', ResultCodes.PARAM_INVALID_VALUE);
    }

    return Department.findById(id);
};

DepartmentManager.prototype.getById = async function (id) {
    console.debug(`getById(), id:${id}`);

    if (!Kinds.isObjectId(id)) {
        throw ResultCodes.newError('Invalid Object Id', ResultCodes.PARAM_INVALID_VALUE);
    }

    const department = await Department.findById(id);
    if (!team) {
        throw ResultCodes.newError(`Team ${id} doesn't exist`, ResultCodes.NO_CONTENT, { id });
    }
    return department;
};
DepartmentManager.prototype.find = async function (filter) {
    console.debug(`find(), filter:${JSON.stringify(filter)}`);

    const highFilter = {};
    if (filter.userIds) {
        highFilter._id = { $in: Kinds.asObjectIds(filter.userIds) };
        //return User.find({ _id: { $in: Kinds.asObjectIds(filter.userIds) } }, '_id displayName avatar');
    }

    filter._id && (highFilter._id = { $in: Kinds.asObjectIds(filter._id) });

    if (filter.text) {
        highFilter.$text = { $search: filter.text };
    }

    const query = SearchStream.of(Department);
    query
        .highFilter(highFilter)
        .resultKey(filter.resultKey)
        .limit(Kinds.asNumber(filter.limit, 10))
        .sort(filter.order);

    return query.exec();
};

DepartmentManager.prototype.update = async function (id, obj) {
    console.debug(`update(), id:${id}`);
    Kinds.mustExist(id);
    Kinds.mustExist(obj);

    const user = await this.getById(id);

    Object.keys(obj).forEach((key) => {
        if (obj[key] === undefined) {
            return;
        }
        if (Kinds.isString(obj[key]) && obj[key].length === 0) {
            return;
        }
        user[key] = obj[key];
    });

    return Department.save();
};

DepartmentManager.prototype.addTeam = async function (obj) {
    console.debug(`addTeam(), team:${JSON.stringify(obj)}`);
    Kinds.mustBeString(obj.department);
    Kinds.mustBeString(obj.team);
    if (!Kinds.isObjectId(obj.department)) {
        throw ResultCodes.newError('Invalid Object Id', ResultCodes.PARAM_INVALID_VALUE);
    }
    if (!Kinds.isObjectId(obj.team)) {
        throw ResultCodes.newError('Invalid Object Id', ResultCodes.PARAM_INVALID_VALUE);
    }
    console.log('DepartmentManager', obj);
    return TeamOfDepartment.create([obj]);
};
DepartmentManager.prototype.getTeamsByDepartmentId = async function (id, filter) {
    console.debug(`addTeam(), team:${JSON.stringify(filter)}`);
    Kinds.mustBeString(id);
    if (!Kinds.isObjectId(id)) {
        throw ResultCodes.newError('Invalid Object Id', ResultCodes.PARAM_INVALID_VALUE);
    }
    const highFilter = {
        department:{ $in: Kinds.asObjectId(id)}
    };
    filter.departmentIds && (highFilter.department = { $in: Kinds.asObjectIds(filter.departmentIds) });


    const query = SearchStream.of(TeamOfDepartment);
    query
        .highFilter(highFilter)
        .lookup({
            from: 'Team',
            localField: 'team',
            foreignField: '_id',
            as: 'teams'
        })
        .resultKey(filter.resultKey)
        .limit(Kinds.asNumber(filter.limit, 10))
        .sort(filter.order);

    return query.exec();

};

DepartmentManager.prototype.removeTeam = async function (_id) {
    console.debug(`addTeam(), team: ${_id}`);
    Kinds.mustBeString(id);
    if (!Kinds.isObjectId(id)) {
        throw ResultCodes.newError('Invalid Object Id', ResultCodes.PARAM_INVALID_VALUE);
    }
    return TeamOfDepartment.deleteOne({_id});
};