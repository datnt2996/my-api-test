const Enums = {};
module.exports = Enums;

Enums.Permissions = {
    SUPER: 'super',
};
Enums.UserRoles = {
    UNKNOWN: 'unknown',
    DIRECTOR: 'director',
    MANAGER: 'manager',
    LEADER: 'leader'
};

Enums.Genders = {
    UNKNOWN: 'unknown',
    MALE: 'male',
    FEMALE: 'female'
};