// @ts-check
// @ts-ignore
const Ini = require('datnt2996/config/ini');
// @ts-ignore
const WebServer = require('./web/server');
const db = require('./db');
const managers = require('./manager');
/**
 * @namespace
 * @name AppCtx
 * @alias AppCtx
 */
const app = {};

const $ = {};
$.load = async (iniFile) => {
    return (
        Ini.readFile(iniFile)
            .then((config) => {
                app.ini = config;
                return app;
            })
            .then(db.load)
            .then(managers.load)
            .then(WebServer.load)
            .then(async (app) => {
                return app;
            })
            .catch((e) => {
                console.error(e);
                process.exit();
            })
    );
};

module.exports = $;
