const { Kinds } = require('datnt2996');
const { Res } = require('datnt2996/koa');
const ResultCodes = require('../result-codes');

const MiddleWares = {};

module.exports = MiddleWares;

let logger= console;

MiddleWares.load = async (app) => {
    return app;
};

MiddleWares.logRequest = async (ctx, next) => {
    const time = Date.now();
    const req = ctx.request;
    logger.info(`${req.method}: ${req.url} < ${JSON.stringify(req.body)}`);

    return next()
        .catch((err) => {
            logger.debug(`${req.method}: ${req.url} [${Date.now() - time}ms] >> error!!! `);
            logger.debug(err);
            return Promise.reject(err);
        })
        .then(() => {
            logger.debug(`${req.method}: ${req.url} [${Date.now() - time}ms] > ${JSON.stringify(ctx.response.body)}`);
        });
};
