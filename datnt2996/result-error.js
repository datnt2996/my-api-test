class ResultError extends Error {
    constructor(name, resultCode, resultDetail) {
        super(name);
        this.resultCode = resultCode;
        this.resultData = resultDetail;
    }
}

module.exports = ResultError;
