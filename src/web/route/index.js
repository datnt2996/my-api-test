const Router = require('koa-router');
const MiddleWares = require('../middlewares');
const Enums = require('../../db/enums');

const router = new Router();
module.exports = router;
router.get('/', require('./info'));

// USER ==========================================
router.get('/users', require('./users/find'));
router.get('/users/:id', require('./users/get'));
router.put('/users/:id', require('./users/update'));
router.post('/users', require('./users/create'));

//
router.get('/teams', require('./teams/find'));
router.get('/teams/:id', require('./teams/get'));
router.put('/teams/:id', require('./teams/update'));
router.post('/teams', require('./teams/create'));
//
router.get('/departments', require('./departments/find'));
router.get('/departments/:id', require('./departments/get'));
router.put('/departments/:id', require('./departments/update'));
router.post('/departments', require('./departments/create'));

//
router.get('/user-groups', require('./user-groups/find'));
router.get('/user-groups/:id', require('./user-groups/get'));
router.put('/user-groups/:id', require('./user-groups/update'));
router.post('/user-groups', require('./user-groups/create'));