
var os = require("os");
/**
 *
 * @param ctx
 * @returns {Promise<void>}
 */
module.exports = async (ctx) => {
    const { appCtx } = ctx.state;
    const { code } = ctx.request.query;
    return {
        app: "test api by datnt2996",
        env: process.env.env,
        headers: ctx.request.headers,
        hostname: os.hostname()
    };
};
