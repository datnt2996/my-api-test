
class Base64  {
    static encode(obj) {
        return Buffer.from(JSON.stringify(obj)).toString('base64');
    }
    static decode (base64) {
        return JSON.parse(Buffer.from(base64, 'base64').toString('utf8'));
    }
}

module.exports = Base64;
