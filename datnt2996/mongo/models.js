const Kinds = require('../kinds');
const Models = {};
module.exports = Models;

/**
 * @param {Object} model
 * @param {Object} source
 * @param {Object} dest
 * @param {Array<String>} excludes
 */
Models.copyAvailableFields = (model, source, dest, excludes = []) => {
    Kinds.mustExist(model);
    Kinds.mustExist(source);
    Kinds.mustExist(dest);

    model.eachPath((field, value) => {
        if (source[field] !== undefined && !excludes.includes(field)) {
            dest[field] = source[field];
        }
    });
};
