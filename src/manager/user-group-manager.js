const axios = require('axios');
const db = require('../db');
const { Team, User, Department, MemberOfTeam, UserGroup, Group, TeamOfDepartment } = db;
const { SearchStream } = require('datnt2996/mongo');
const { Kinds } = require('datnt2996');
const ResultCodes = require('../result-codes');
const Enums = require('../db/enums');

const UserGroupManager = function (app) {
    this.app = app;
};
module.exports = UserGroupManager;


UserGroupManager.prototype.create = async function (obj) {
    console.debug(`create(), team:${JSON.stringify(obj)}`);

    Kinds.mustBeObject(obj);
    console.log('DepartmentManager', obj);
    return UserGroup.create([obj]);
};

UserGroupManager.prototype.findById = async function (id) {
    console.debug(`findById(), id:${id}`);

    if (!Kinds.isObjectId(id)) {
        throw ResultCodes.newError('Invalid Object Id', ResultCodes.PARAM_INVALID_VALUE);
    }

    return UserGroup.findById(id);
};

UserGroupManager.prototype.getById = async function (id) {
    console.debug(`getById(), id:${id}`);

    if (!Kinds.isObjectId(id)) {
        throw ResultCodes.newError('Invalid Object Id', ResultCodes.PARAM_INVALID_VALUE);
    }

    const department = await UserGroup.findById(id);
    if (!team) {
        throw ResultCodes.newError(`Team ${id} doesn't exist`, ResultCodes.NO_CONTENT, { id });
    }
    return department;
};
UserGroupManager.prototype.find = async function (filter) {
    console.debug(`find(), filter:${JSON.stringify(filter)}`);

    const highFilter = {};
    if (filter.userIds) {
        highFilter._id = { $in: Kinds.asObjectIds(filter.userIds) };
        //return User.find({ _id: { $in: Kinds.asObjectIds(filter.userIds) } }, '_id displayName avatar');
    }

    filter._id && (highFilter._id = { $in: Kinds.asObjectIds(filter._id) });

    if (filter.text) {
        highFilter.$text = { $search: filter.text };
    }

    const query = SearchStream.of(UserGroup);
    query
        .highFilter(highFilter)
        .resultKey(filter.resultKey)
        .limit(Kinds.asNumber(filter.limit, 10))
        .sort(filter.order);

    return query.exec();
};

UserGroupManager.prototype.update = async function (id, obj) {
    console.debug(`update(), id:${id}`);
    Kinds.mustExist(id);
    Kinds.mustExist(obj);

    const user = await this.getById(id);

    Object.keys(obj).forEach((key) => {
        if (obj[key] === undefined) {
            return;
        }
        if (Kinds.isString(obj[key]) && obj[key].length === 0) {
            return;
        }
        user[key] = obj[key];
    });

    return UserGroup.save();
};

