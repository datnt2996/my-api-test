
module.exports = async (ctx) => {
    const { appCtx } = ctx.state;
    const { userManager } = appCtx.managers;

    return userManager.create(ctx.request.body);
};
