
module.exports = async (ctx) => {
    const { appCtx } = ctx.state;
    const { userGroupManager } = appCtx.managers;

    return userGroupManager.create(ctx.request.body);
};
