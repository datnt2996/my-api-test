const mongoose = require('mongoose');
const { Kinds } = require('datnt2996');
const Enums = require('./enums');

const {
    User,
    UserGroup,
    Group,
    Department,
    Team,
    MemberOfTeam,
    TeamOfDepartment
} = require('./schema');

/**
 * @namespace db
 */
const db = {};
module.exports = db;

db.User = mongoose.model('User', User);
db.UserGroup = mongoose.model('UserGroup', UserGroup);
db.Group = mongoose.model('Group', Group);
db.Department = mongoose.model('Department', Department);
db.Team = mongoose.model('Team', Team);
db.MemberOfTeam = mongoose.model('MemberOfTeam', MemberOfTeam);
db.TeamOfDepartment = mongoose.model('TeamOfDepartment', TeamOfDepartment);

mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('debug', true);

db.load = async function (app) {
    const url = app.ini.mongo.url;
    const dbName = app.ini.mongo.db;
    const poolSize = app.ini.mongo.poolSize ? Kinds.asNumber(app.ini.mongo.poolSize) : 2;
    Kinds.mustGreaterThan(poolSize, 0);

    return mongoose
        .connect(url, {
            useNewUrlParser: true,
            dbName,
            poolSize,
            autoIndex: app.ini.mongo.autoIndex == true // ignore type comparision
        })
        .then(async () => {
            console.info('mongo connected');
            app.db = db;

            await db.createCollections();

            return app;
        });
};

db.createCollections = async () => {
    for await (const name of Kinds.objectKeys(db)) {
        if (db.hasOwnProperty(name) && db[name].collection && Kinds.isFunction(db[name].createCollection)) {
            await db[name].createCollection();
        }
    }
};

db.startTransaction = async () => {
    const ss = await mongoose.startSession();
    await ss.startTransaction();

    ss.__listeners = [];

    ss.onTransactionCommitted = function (func) {
        ss.__listeners.push(func);
    };
    return ss;
};

db.doneTransaction = async (session) => {
    await session.commitTransaction();

    if (Kinds.isArray(session.__listeners)) {
        session.__listeners.forEach(async (listener) => {
            try {
                const result = listener();
                if (result && Kinds.isFunction(result.then)) {
                    await result;
                }
            } catch (e) {
                console.error(e);
            }
        });
    }

    return session.endSession();
};

db.abortTransaction = async (session) => {
    await session.abortTransaction();
    return session.endSession();
};

db.closeConnection = async () => {
    return mongoose.connection.close();
}