const path = require('path');
const Koa = require('koa');
const koaBody = require('koa-body');
const render = require('koa-ejs');
const koaJson = require('koa-json');
const { Res } = require('datnt2996/koa');
const { ObjectId } = require('mongoose').Types;
const ResultCodes = require('../result-codes');
const koaStatic = require('koa-static');
const koaMount = require('koa-mount');
const cors = require('@koa/cors');
const Utils = require('../lib/utils');
const i18n = require("i18n");

i18n.configure({
    locales: ['en', 'vi'],
    directory: __dirname + '/../locales'
});

const $ = {};
module.exports = $
let app;
let errorToken = {};

const webServer = new Koa();

webServer.use(cors());
webServer.use(koaBody({ multipart: true }));
webServer.use(koaJson());
render(webServer, {
    root: path.join(__dirname, 'pages'),
    layout: false,
    viewExt: 'html',
    cache: false,
    debug: false
});

staticServe = (path, route = '/') => koaMount(route, new Koa().use(koaStatic(path)));

// webServer.use(cors());
webServer.use(staticServe('./src/web/assets', '/assets'));

webServer.use(async (ctx, next) => {
    if (ctx.request.method === 'GET') {
        ctx.request.query = Utils.removeBracketInKeys(ctx.request.query);
    }
    return next();
});

webServer.use(async (ctx, next) => {
    const logger = console;
    logger.debug(`populateUser()`);

    return next();
})




webServer.use(async (ctx, next) => {
    const headers = ctx.request.headers;
    const appCtx = {
        ...app
    };

    appCtx.managers = app.managers.build(appCtx);

    appCtx.i18n = i18n;
    ctx.state.appCtx = appCtx;

    const logger = console;

    const t = Date.now();
    logger.info(`${ctx.request.method} ${ctx.request.url} <  ${JSON.stringify(ctx.request.body)}`);
    if (errorToken.length) {
        console.log("error: ", errorToken);
        throw ResultCodes.newError(errorToken.message, errorToken.code);
    }
    try {
        const result = await next();
        if (ctx.response.body) {
        } else if (result !== undefined) {
            // logger.debug(result);
            Res(ctx).ok('ok', result);
        }

        logger.info(`${ctx.request.method} ${ctx.request.url} s:${ctx.response.status} ${Date.now() - t}ms >  ${JSON.stringify(ctx.response.body)}`);
    } catch (e) {
        if (e.resultCode) {
            logger.warn(
                `${ctx.request.method} ${ctx.request.url} s:${ctx.response.status} ${Date.now() - t}ms >  ${e.resultCode}, message: ${e.message}, ${JSON.stringify(e.resultData)}`
            );
            return Res(ctx).bad(e.resultCode, e.message, e.resultData);
        }
        logger.error(`${ctx.request.method} ${ctx.request.url} s:${ctx.response.status} ${Date.now() - t}ms >  Internal Error`);

        Res(ctx).bad(ResultCodes.ERROR, e.message, { stack: e.stack });
    }
});


const allRoutes = require('./route');

webServer.use(allRoutes.routes());

$.load = async (appInstance) => {
    app = appInstance;
    const host = app.ini.server.host || '0.0.0.0';
    const port = app.ini.server.port || 2996;
    app.web = webServer;

    return new Promise((resolve, reject) => {
        webServer.listen(port, host, (err) => {
            if (err) {
                return reject(err);
            }

            console.info(`Web server is listening at http://${host}:${port}`);

            resolve(app);
        });
    });
};
