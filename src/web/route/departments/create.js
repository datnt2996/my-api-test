
module.exports = async (ctx) => {
    const { appCtx } = ctx.state;
    const { departmentManager } = appCtx.managers;

    return departmentManager.create(ctx.request.body);
};
