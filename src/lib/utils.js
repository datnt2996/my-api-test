const { Kinds } = require('datnt2996');
const ResultCodes = require('../result-codes');
const moment = require('moment');
const { stringify } = require('ini');
const Enums = require('../db/enums');

const $ = {};
module.exports = $;

/**
 * {"name[]": [1,2]} => {"name": [1,2]}
 */

$.removeBracketInKeys = function (query) {
    if (query) {
        Kinds.objectKeys(query).forEach((key) => {
            if (/\[\]$/.test(key)) {
                query[key.substring(0, key.length - 2)] = query[key];
            }
        });
    }
    return query || {};
};


$.changeFieldsExportsCsv = async function (data, project) {
    if (!project) {
        project = {
            _id: 1
        }
    }


    let fieldKeys = Object.keys(project);

    console.log(fieldKeys);

    const results = data.map((item) => {
        fieldKeys.forEach((key) => {
            if (!item[key]) {
                item[key] = "";
            }
            if (Kinds.isArray(item[key])) {
                item[key] = item[key] && item[key].length > 0 ? item[key].join(", ") : '';
            }
            if (Kinds.isDate(item[key])) {
                item[key] = moment(item[key]).format("YYYY-MM-DD hh:mm:ss");
            }
        })
        return item;
    })

    return results;

};


$.formatDateToString = async function (date) {
    if (date) {
        return moment(date).format("YYYY-MM-DD");
    }
    return null;
};

$.formatStringToDate = async function (date) {
    if (date && Kinds.isString(date)) {
        return new Date(date).toJSON();
    }
    return date;
};

