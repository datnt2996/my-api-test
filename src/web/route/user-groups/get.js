const { Res } = require('datnt2996/koa');
const Enums = require('../../../db/enums');
const { Kinds } = require('datnt2996');
const ResultCodes = require('../../../result-codes');

module.exports = async (ctx) => {
    const { appCtx } = ctx.state;
    const {userGroupManager} = appCtx.managers;
    
    const res = await userGroupManager.getById(ctx.params.id);
    return Res(ctx).ok('success', res);
};
