const { Res } = require('datnt2996/koa');
const Enums = require('../../../db/enums');
const { Kinds } = require('datnt2996');
const ResultCodes = require('../../../result-codes');

module.exports = async (ctx) => {
    const { appCtx } = ctx.state;
    const {departmentManager} = appCtx.managers;
    
    const res = await departmentManager.update(ctx.params.id, ctx.body);
    return Res(ctx).ok('success', res);
};
