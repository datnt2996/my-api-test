const axios = require('axios');
const db = require('../db');
const { Team, User, Department, MemberOfTeam, UserGroup, Group, TeamOfDepartment } = db;
const { SearchStream } = require('datnt2996/mongo');
const { Kinds } = require('datnt2996');
const ResultCodes = require('../result-codes');
const Enums = require('../db/enums');

const UserManager = function (app) {
    this.app = app;
};
module.exports = UserManager;


UserManager.prototype.create = async function (profile) {
    console.debug(`create(), profile:${JSON.stringify(profile)}`);

    Kinds.mustBeObject(profile);
    console.log('user', profile);
    return User.create([profile]);
};

UserManager.prototype.findById = async function (id) {
    console.debug(`findById(), id:${id}`);

    if (!Kinds.isObjectId(id)) {
        throw ResultCodes.newError('Invalid Object Id', ResultCodes.PARAM_INVALID_VALUE);
    }

    return User.findById(id);
};
UserManager.prototype.getById = async function (id) {
    console.debug(`getById(), id:${id}`);

    if (!Kinds.isObjectId(id)) {
        throw ResultCodes.newError('Invalid Object Id', ResultCodes.PARAM_INVALID_VALUE);
    }

    const user = await User.findById(id);
    if (!user) {
        throw ResultCodes.newError(`User ${id} doesn't exist`, ResultCodes.NO_CONTENT, { id });
    }
    return user;
};

UserManager.prototype.searchUser = async function (filter) {
    console.debug(`searchUser(), filter:${JSON.stringify(filter)}`);

    const highFilter = {};
    if (filter.userIds) {
        highFilter._id = { $in: Kinds.asObjectIds(filter.userIds) };
    }

    filter._id && (highFilter._id = { $in: Kinds.asObjectIds(filter._id) });
    filter.statuses && (highFilter.status = { $in: Kinds.asArray(filter.statuses) });
    filter.genders && (highFilter.gender = { $in: Kinds.asArray(filter.genders) });

    if (filter.text) {
        highFilter.$text = { $search: filter.text };
    }

    const query = SearchStream.of(User);
    query
        .highFilter(highFilter)
        .resultKey(filter.resultKey)
        .limit(Kinds.asNumber(filter.limit, 10))
        .sort(filter.order);

    return query.exec();
};

UserManager.prototype.update = async function (id, obj) {
    console.debug(`update(), id:${id}`);
    Kinds.mustExist(id);
    Kinds.mustExist(obj);

    const user = await this.getById(id);

    delete obj.phone;
    delete obj.email;

    Object.keys(obj).forEach((key) => {
        if (obj[key] === undefined) {
            return;
        }
        if (Kinds.isString(obj[key]) && obj[key].length === 0) {
            return;
        }
        user[key] = obj[key];
    });

    return user.save();
};

